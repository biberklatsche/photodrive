package de.diemuehlrams.photodrive.exception;

import org.apache.log4j.Logger;

public class PDException extends Exception {

	private PDException(String message, Exception ex) {
		super(message, ex);
	}

	private PDException(String message) {
		super(message);
	}

	public static PDException createAndLog(Logger log, String message) {
		return createAndLog(log, message, new Object[0]);
	}

	public static PDException createAndLog(Logger log, String message,
			Object... arguments) {
		if (message == null) {
			message = "";
		}
		if (log == null) {
			log = Logger.getLogger("");
		}
		String errorMessage = createMessage(message, arguments);
		log.error(errorMessage);
		Exception ex = getException(arguments);
		if (ex != null) {
			return new PDException(errorMessage, ex);
		} else {
			return new PDException(errorMessage);
		}
	}

	private static Exception getException(Object... ex) {
		if (ex != null && ex.length > 0
				&& ex[ex.length - 1] instanceof Exception) {
			return (Exception) ex[ex.length - 1];
		} else {
			return null;
		}
	}

	private static String createMessage(String message, Object... arguments) {
		try {
			return String.format(message, arguments);
		} catch (Exception ex) {
			return message;
		}
	}

}
