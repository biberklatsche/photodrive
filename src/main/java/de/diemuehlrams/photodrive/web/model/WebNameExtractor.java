package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;

/**
 * Ermittelt den Namen aus einer Datei oder Ordner, so wie er auf der Webseite dargestellt werden soll.
 * @author larswolfram
 */
public class WebNameExtractor{

    public String create(File file) {
        if(file == null){
            return "";
        }
        String fileName = file.getName();
        if (file.isFolder()) {
            fileName = fileName.replaceAll("^[0-9]+\\.(\\s)*", "");
        }else if(fileName.contains(".")){
            String nameWithoutEnding = fileName.substring(0, fileName.lastIndexOf("."));
            fileName = nameWithoutEnding.replaceAll("^[0-9]+\\.(\\s)*", "");
        }
        return fileName.replaceAll("_", " ");
    }
}
