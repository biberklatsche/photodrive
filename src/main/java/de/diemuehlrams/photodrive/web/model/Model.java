package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.exception.PDException;
import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.service.FileService;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Lars
 */
public class Model {

    private final List<WebElement> elements = new java.util.concurrent.CopyOnWriteArrayList();
    private final Random random = new Random();
    private static volatile Model INSTANCE = null;
    private static final int PROOF_UPDATE_TIME_IN_MILLISECONDS = 30 * 1000;
    private static int proofUpdateCounter = 0;
    private static final Logger LOG = Logger
            .getLogger(Model.class);
    private final Timer timer = new Timer();

    @Autowired
    private FileService fileService;

    @Autowired
    private Transformer transformer;

    private Model() {
    }

    public static Model getInstance() {
        if (INSTANCE == null) {
            synchronized (Model.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Model();
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        timer.schedule(new UpdateModelTask(), 1000, PROOF_UPDATE_TIME_IN_MILLISECONDS);
    }

    @PreDestroy
    private void clean() {
        timer.cancel();
    }

    /**
     * @param from
     * @param offset
     * @return the files
     */
    public WebElements getWebElements(int from, int offset) {
        int to = from + offset;
        if (from > elements.size()) {
            from = elements.size();
        }
        if (to > elements.size()) {
            to = elements.size();
        }
        List<WebElement> we = elements.subList(from, to);
        WebElements results = new WebElements();
        results.setElements(we);
        results.setMaxCount(getElementsCount());
        return results;
    }

    /**
     * @param from
     * @param offset
     * @return the files
     */
    List<WebElement> getElements(int from, int offset) {
        int to = from + offset;
        if (from > elements.size()) {
            from = elements.size();
        }
        if (to > elements.size()) {
            to = elements.size();
        }
        return elements.subList(from, to);
    }

    public int getElementsCount() {
        return elements.size();
    }

    void setElements(List<WebElement> elements) {
        this.elements.clear();
        this.elements.addAll(elements);
    }

    public void update() {
        try {
            LOG.debug("Reload Model");
            File root = fileService.getRoot();
            List<WebElement> webElements = transformer.run(root);
            setElements(webElements);
        } catch (PDException ex) {
            LOG.error("Could not load files from google drive", ex);
        }
    }

    public boolean hasChanged() {
        String urlAsString = getUrlToTest();
        if (!urlAsString.isEmpty()) {
            try {
                URL url = new URL(urlAsString);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                int responseCode = urlConnection.getResponseCode();
                return responseCode != 200;
            } catch (IOException ex) {
                return false;
            }
        }else{
            return false;
        }
    }

    private String getUrlToTest() {
        Integer randomIndex;
        int counter = 0;
        do {
            randomIndex = random.nextInt(elements.size());
            counter++;
        } while (!elements.get(randomIndex).getType().equals(WebElement.IMAGE) && counter < 20);
        return elements.get(randomIndex).getThumbnail();
    }

    private class UpdateModelTask extends TimerTask {

        @Override
        public void run() {
            LOG.debug(String.format("Proof update Model. Counter = %s", proofUpdateCounter));
            if (proofUpdateCounter == 0 || hasChanged()) {
                LOG.info(String.format("Update Model. Counter = %s", proofUpdateCounter));
                update();
            }
            proofUpdateCounter = ++proofUpdateCounter % 120;
        }
    }
}
