package de.diemuehlrams.photodrive.web.model;

import java.util.List;

/**
 *
 * @author Lars
 */
public class WebElements {
    private List<WebElement> elements;
    private int maxCount;

    public List<WebElement> getElements() {
        return elements;
    }

    public void setElements(List<WebElement> elements) {
        this.elements = elements;
    }
    
    /**
     * @return the count
     */
    public int getMaxCount() {
        return maxCount;
    }

    /**
     * @param count the count to set
     */
    public void setMaxCount(int count) {
        this.maxCount = count;
    }
    
    /**
     *
     * @return
     */
    public int size(){
        return elements.size();
    }
}
