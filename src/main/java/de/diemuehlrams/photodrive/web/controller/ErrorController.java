package de.diemuehlrams.photodrive.web.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 *
 * @author Lars
 */
@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handle(Exception ex) {
        return "login";
    }
}
