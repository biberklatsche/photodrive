/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.diemuehlrams.photodrive.web.controller;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author wolframl
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String root(Principal principal) {
        return "redirect:/index.html";
    }
    
    @RequestMapping(value = "/index.html")
    public ModelAndView index(Principal principal) {
        if(principal == null){
            return new ModelAndView("redirect:/login.html");
        }
        ModelAndView mv = new ModelAndView("private/index");
        return mv;
    }
}
