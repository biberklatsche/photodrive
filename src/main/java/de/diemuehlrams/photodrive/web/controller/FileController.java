package de.diemuehlrams.photodrive.web.controller;

import de.diemuehlrams.photodrive.web.model.WebElements;
import de.diemuehlrams.photodrive.exception.PDException;
import de.diemuehlrams.photodrive.web.model.Model;
import de.diemuehlrams.photodrive.web.model.WebElement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileController {

    @Autowired
    private Model model;

    @RequestMapping("/private/elements")
    public WebElements elements(@RequestParam("from") int from,
            @RequestParam("offset") int offset) throws PDException {
        WebElements elements = model.getWebElements(from, offset);
        return elements;
    }

    @RequestMapping("/private/reload")
    public String reload() throws PDException {
        model.update();
        return null;
    }
}
