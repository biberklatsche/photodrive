/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.diemuehlrams.photodrive.web.controller;

import com.google.api.client.util.IOUtils;
import de.diemuehlrams.photodrive.exception.PDException;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author larswolfram
 */
@Controller
public class LokalImageController {
    
    private static final Logger LOG = Logger
            .getLogger(LokalImageController.class);
    
    private static final String PATH_TO_ROOT = "/Users/larswolfram/Desktop/Muehlrams/Web";
    
    @RequestMapping("/private/image")
    public void image(@RequestParam("path") String path, HttpServletResponse response) throws PDException {
        try {
            String encodedPath = new String(Base64.getUrlDecoder().decode(path),"utf8");
            File f = new File(PATH_TO_ROOT + encodedPath);
            response.setContentType("image/jpeg");
            try (FileInputStream is = new FileInputStream(f)){
                IOUtils.copy(is, response.getOutputStream());
            }
        } catch (Exception ex) {
            LOG.error("Unable to read image.", ex);
        }
    }
}
