package de.diemuehlrams.photodrive.service;

import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.exception.PDException;
import de.diemuehlrams.photodrive.files.File;

/**
 *
 * @author wolframl
 */

public interface FileService {

    public File getRoot() throws PDException;
}
