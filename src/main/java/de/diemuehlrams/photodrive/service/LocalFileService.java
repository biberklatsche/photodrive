package de.diemuehlrams.photodrive.service;

import de.diemuehlrams.photodrive.exception.PDException;
import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.files.FileFactory;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lars
 */
//@Primary
@Service
public class LocalFileService implements FileService {

    private static final Logger LOG = Logger
            .getLogger(LocalFileService.class);
    
    private static final String PATH_TO_ROOT = "/Users/larswolfram/Desktop/Muehlrams/Web";
    
    @Override
    public File getRoot() throws PDException {
        File root = FileFactory.createRoot();
        fillFolder(new java.io.File(PATH_TO_ROOT), root);
        return root;
    }
    
    private void fillFolder(java.io.File jFile, File folder){
        java.io.File[] jFileChilds = jFile.listFiles();
        if(jFileChilds != null){
            for(java.io.File jFileChild : jFileChilds){
                if(jFileChild.isDirectory()){
                    File childFolder = FileFactory.createFolder(jFileChild.getName(), folder);
                    fillFolder(jFileChild,childFolder);
                }else if(isImageFile(jFileChild)){
                    try {
                        File file = FileFactory.createImageFile(jFileChild.getName(), folder);
                        String filePath = jFileChild.getAbsolutePath().replaceAll(PATH_TO_ROOT, "");
                        String base64EncodedPath = Base64.getUrlEncoder().encodeToString(filePath.getBytes("utf8"));
                        file.setThumbnailLink("./private/image?path=" + base64EncodedPath);
                        file.setLink("./private/image?path="  + base64EncodedPath);
                    } catch (UnsupportedEncodingException ex) {
                        LOG.error("Could not read image path.", ex);
                    }
                }else if(isTxtFile(jFileChild)){
                    File file = FileFactory.createTextFile(jFileChild.getName(), folder);
                    try {
                        String t = new String(com.google.common.io.Files.toByteArray(jFileChild),"utf8");
                        file.setText(t);
                    } catch (IOException ex) {
                        LOG.error("Could not read file.", ex);
                    }
                }
            }
        }
    }

    private boolean isImageFile(java.io.File jFileChild) {
        return jFileChild.isFile() && 
                (jFileChild.getName().toLowerCase().endsWith("jpg")) ||
                (jFileChild.getName().toLowerCase().endsWith("png"));
    }

    private boolean isTxtFile(java.io.File jFileChild) {
        return jFileChild.isFile() && 
                (jFileChild.getName().toLowerCase().endsWith("txt"));    
    }

}
