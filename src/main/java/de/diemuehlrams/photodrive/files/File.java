package de.diemuehlrams.photodrive.files;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Lars
 */
public class File{

    private String name;
    private String thumbnailLink;
    private String link;
    private FileType type;
    private String text;
    private List<File> children;
    private File parent;
    private Long creationTimestamp;

    File(){};
    
    /**
     * @return the url
     */
    public String getThumbnailLink() {
        return thumbnailLink;
    }

    /**
     * @param thumbnailLink the url to set
     */
    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    /**
     * @return the name
     */
    public String getName() {
        if(name == null){
            return "";
        }
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public FileType getType() {
        return type;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
    
    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @param type the type to set
     */
    public void setType(FileType type) {
        this.type = type;
    }

    /**
     * @return the children
     */
    public List<File> getChildren() {
        if(children == null){
            return Collections.emptyList();
        }
        return children;
    }
    
    public void addChild(File child){
        if(!this.isFolder()){
            throw new IllegalArgumentException("Could not add a folder to a file.");
        }
        if(child.isRoot()){
            throw new IllegalArgumentException("Could not add a root-folder to a folder.");
        }
        if(children == null){
            children = new LinkedList();
        }
        this.children.add(child);
    }

    /**
     * @return the parent
     */
    public File getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(File parent) {
        if(parent == null){
            throw new IllegalArgumentException("Null parent folder not allowed.");
        }
        if(!parent.isFolder()){
            throw new IllegalArgumentException("File as parent not allowed.");
        }
        this.parent = parent;
    }

    public boolean isRoot() {
        return getParent() == null;
    }

    public boolean isFolder() {
        return type.equals(FileType.FOLDER);
    }

    public boolean isImage() {
        return type.equals(FileType.IMAGE);
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean isText() {
        return type.equals(FileType.TEXT);
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the creationTimestamp
     */
    public Long getCreationTimestamp() {
        if(creationTimestamp == null){
            return 0l;
        }
        return creationTimestamp;
    }

    /**
     * @param creationTimestamp the creationTimestamp to set
     */
    public void setCreationTimestamp(Long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}
