package de.diemuehlrams.photodrive.files;

/**
 *
 * @author larswolfram
 */
public class FileFactory {
    public static File createFolder(String name, File parent){
        return create(FileType.FOLDER, name, parent);
    }
    
    public static File createImageFile(String name, File parent){
        return create(FileType.IMAGE, name, parent);
    }
    
    public static File createTextFile(String name, File parent) {
        return create(FileType.TEXT, name, parent);
    }
    
    public static File createRoot(){
        File file = new File();
        file.setType(FileType.FOLDER);
        file.setName("/");
        return file;
    }
    
    private static File create(FileType type, String name, File parent){
        File file = new File();
        file.setName(name);
        file.setType(type);
        file.setParent(parent);
        parent.addChild(file);
        return file;
    }
}
