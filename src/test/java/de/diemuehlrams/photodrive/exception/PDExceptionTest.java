package de.diemuehlrams.photodrive.exception;

import org.apache.log4j.Logger;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author lwo
 */
public class PDExceptionTest {

	@Test
	public void testCreateAndLog_allNull_twoArguments() {
		PDException ex = PDException.createAndLog(null, null);
		assertNotNull(ex);

	}

	@Test
	public void testCreateAndLog_allNull_threeArguments() {
		Logger log = null;
		String message = null;
		Exception exeption = null;
		PDException ex = PDException.createAndLog(log, message, exeption);
		assertNotNull(ex);
	}

	@Test
	public void testCreateAndLog_wrongStringFormat() {
		PDException ex = PDException.createAndLog(null, "Hallo %d", "test");
		assertNotNull(ex);
	}

	@Test
	public void testCreateAndLog_simpleMessage() {
		PDException ex = PDException.createAndLog(null, "Hallo");
		assertEquals("Hallo", ex.getMessage());
		assertNull(ex.getCause());
	}

	@Test
	public void testCreateAndLog_complexMessage() {
		PDException ex = PDException.createAndLog(null, "Hallo %s", "test");
		assertEquals("Hallo test", ex.getMessage());
		assertNull(ex.getCause());
	}

	@Test
	public void testCreateAndLog_simpleMessageWithException() {
		PDException ex = PDException.createAndLog(null, "Hallo",
				new Exception());
		assertEquals("Hallo", ex.getMessage());
		assertNotNull(ex.getCause());
	}

	@Test
	public void testCreateAndLog_complexMessageWithException() {
		PDException ex = PDException.createAndLog(null, "Hallo %s", "test",
				new Exception());
		assertEquals("Hallo test", ex.getMessage());
		assertNotNull(ex.getCause());
	}

}
