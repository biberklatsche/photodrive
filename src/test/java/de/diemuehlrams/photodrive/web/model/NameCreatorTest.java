package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.files.FileFactory;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author larswolfram
 */
public class NameCreatorTest {

    private WebNameExtractor nameCreator;

    @Before
    public void before() {
        nameCreator = new WebNameExtractor();
    }

    @Test
    public void testWork_null() {
        String actual = nameCreator.create(null);
        assertEquals("", actual);
    }

    @Test
    public void testWork_onlyRoot() {
        File root = FileFactory.createRoot();
        String actual = nameCreator.create(root);
        assertEquals("/", actual);
    }

    @Test
    public void testWork_removeNumbersWithWitespace() {
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("01. Test", root);
        assertEquals("Test", nameCreator.create(folder));
    }

    @Test
    public void testWork_removeNumbers() {
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("01.Test", root);
        assertEquals("Test", nameCreator.create(folder));
    }

    @Test
    public void testWork_dontRemoveIfOnlyNumber() {
        File root = FileFactory.createRoot();
        File file = FileFactory.createImageFile("001.jpg", root);

        assertEquals("001", nameCreator.create(file));
    }
    
    @Test
    public void testWork_imageWithoutEnding() {
        File root = FileFactory.createRoot();
        File file = FileFactory.createImageFile("001", root);

        assertEquals("001", nameCreator.create(file));
    }
    
    @Test
    public void testWork_replaceUnderscore() {
        File root = FileFactory.createRoot();
        File file = FileFactory.createImageFile("Hello_you.jpg", root);

        assertEquals("Hello you", nameCreator.create(file));
    }
}
