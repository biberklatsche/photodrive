/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.diemuehlrams.photodrive.web.model;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Lars
 */
public class ModelTest {

    @Test
    public void testGetFiles_from0To0() {
        Model m = Model.getInstance();
        m.setElements(Arrays.asList(WebElement.createHeader(), WebElement.createHeader(), WebElement.createHeader()));
        List<WebElement> files = m.getElements(0, 0);
        assertEquals(0, files.size());
    }

    @Test
    public void testGetFiles_fromBiggerTo() {
        Model m = Model.getInstance();
        m.setElements(Arrays.asList(WebElement.createHeader(), WebElement.createHeader(), WebElement.createHeader()));
        List<WebElement> files = m.getElements(1, 0);
        assertEquals(0, files.size());
    }

    @Test
    public void testGetFiles_toBiggerListSize() {
        Model m = Model.getInstance();
        m.setElements(Arrays.asList(WebElement.createHeader(), WebElement.createHeader(), WebElement.createHeader()));
        List<WebElement> files = m.getElements(0, 100);
        assertEquals(3, files.size());
    }

    @Test
    public void testGetFiles_fromBiggerListSize() {
        Model m = Model.getInstance();
        m.setElements(Arrays.asList(WebElement.createHeader(), WebElement.createHeader(), WebElement.createHeader()));
        List<WebElement> files = m.getElements(100, 200);
        assertEquals(0, files.size());
    }
}
