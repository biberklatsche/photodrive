package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.files.FileFactory;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author larswolfram
 */
public class TransformerTest {
    
    private static Transformer testObject;
    
    @BeforeClass
    public static void before(){
        testObject = new Transformer();
        testObject.setSorter(new Sorter());
        testObject.setNameCreator(new WebNameExtractor());
    }
    
    @Test
    public void testTransorm_null(){
        assertEquals(0,testObject.run(null).size());
    }
    
    @Test
    public void testTransorm_onlyRoot(){
        File root = FileFactory.createRoot();
        assertEquals(0,testObject.run(root).size());
    }
    
    @Test
    public void testTransorm_oneFolder(){
        File root = FileFactory.createRoot();
        FileFactory.createFolder("2015",root);
        List<WebElement> elements = testObject.run(root);
        assertEquals("2015",elements.get(0).getYear());
    }
    
    @Test
    public void testTransorm_twoFolders(){
        File root = FileFactory.createRoot();
        FileFactory.createFolder("2015",root);
        FileFactory.createFolder("2016",root);
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2016",elements.get(0).getYear());
        assertEquals(WebElement.HEADER,elements.get(1).getType());
        assertEquals("2015",elements.get(1).getYear());
    }
    
    @Test
    public void testTransorm_oneSubFolder(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        FileFactory.createFolder("Januar",folder);
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2015",elements.get(0).getYear());
        assertEquals("Januar",elements.get(0).getHeaderName());
    }
    
    @Test
    public void testTransorm_twoSubFolders(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        FileFactory.createFolder("Januar",folder);
        FileFactory.createFolder("Februar",folder);
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2015",elements.get(0).getYear());
        assertEquals("Januar",elements.get(0).getHeaderName());
        
        assertEquals(WebElement.SUB_HEADER,elements.get(1).getType());
        assertEquals("2015",elements.get(1).getYear());
        assertEquals("Februar",elements.get(1).getHeaderName());
    }
    
    @Test
    public void testTransorm_oneSubSubFolder(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        File subFolder = FileFactory.createFolder("Frankreich",folder);
        FileFactory.createFolder("Merzien",subFolder);
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2015",elements.get(0).getYear());
        assertEquals("Frankreich",elements.get(0).getHeaderName());
        assertEquals("Merzien",elements.get(0).getHeaderSubname());
    }
    
    @Test
    public void testTransorm_twoSubSubFolders(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        File subFolder = FileFactory.createFolder("Frankreich",folder);
        FileFactory.createFolder("Leipzig",subFolder);
        FileFactory.createFolder("Merzien",subFolder);
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2015",elements.get(0).getYear());
        assertEquals("Frankreich", elements.get(0).getHeaderName());
        assertEquals("Leipzig",elements.get(0).getHeaderSubname());
        assertEquals("Frankreich",elements.get(1).getHeaderName());
        assertEquals("Merzien",elements.get(1).getHeaderSubname());
    }
    
    @Test
    public void testTransorm_complexFolderStructure(){
        File root = FileFactory.createRoot();
        File folder1 = FileFactory.createFolder("2015",root);
        File subFolder1 = FileFactory.createFolder("Frankreich",folder1);
        FileFactory.createFolder("Leipzig",subFolder1);
        FileFactory.createFolder("Merzien",subFolder1);
        FileFactory.createFolder("Januar",folder1);
        File folder2 = FileFactory.createFolder("2016",root);
        FileFactory.createFolder("Januar",folder2);
        
        List<WebElement> elements = testObject.run(root);
        assertEquals(WebElement.HEADER,elements.get(0).getType());
        assertEquals("2016",elements.get(0).getYear());
        assertEquals("Januar",elements.get(0).getHeaderName());
        assertEquals("2015",elements.get(1).getYear());
        assertEquals("Januar",elements.get(1).getHeaderName());
        assertEquals("Frankreich",elements.get(2).getHeaderName());
        assertEquals("Leipzig",elements.get(2).getHeaderSubname());     
        assertEquals("Frankreich",elements.get(3).getHeaderName());
        assertEquals("Merzien",elements.get(3).getHeaderSubname());
        
    }
    
    @Test
    public void testTransorm_oneImageInFolder(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        File subFolder = FileFactory.createFolder("Januar",folder);
        FileFactory.createImageFile("1",subFolder);
        List<WebElement> elements = testObject.run(root);
        assertEquals("1",elements.get(1).getText());
    }
    
    @Test
    public void testTransorm_twoImagesInFolder(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        File subFolder = FileFactory.createFolder("Januar",folder);
        FileFactory.createImageFile("1",subFolder);
        FileFactory.createImageFile("2",subFolder);
        List<WebElement> elements = testObject.run(root);
        assertEquals("1",elements.get(1).getText());
        assertEquals("2",elements.get(2).getText());
    }
    
    @Test
    public void testTransorm_withDescription(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        File textFile = FileFactory.createTextFile("!description", folder);
        textFile.setText("Description");
        
        List<WebElement> elements = testObject.run(root);
        
        assertEquals("2015",elements.get(0).getYear());
        assertEquals("Description",elements.get(0).getText());
    }
    
    @Test
    public void testTransorm_complexCompleteStructure(){
        File root = FileFactory.createRoot();
        File folder1 = FileFactory.createFolder("2015",root);
        File subFolder1 = FileFactory.createFolder("Frankreich",folder1);
        File subSubFolder1 = FileFactory.createFolder("Leipzig",subFolder1);
        File textFile = FileFactory.createTextFile("!description", subSubFolder1);
        textFile.setText("Description");
        FileFactory.createImageFile("1",subSubFolder1);
        FileFactory.createImageFile("2",subSubFolder1);
        File subSubFolder2 = FileFactory.createFolder("Merzien",subFolder1);
        FileFactory.createImageFile("3",subSubFolder2);
        FileFactory.createFolder("Januar",folder1);
        File folder2 = FileFactory.createFolder("2016",root);
        File subFolder2 = FileFactory.createFolder("Dezember",folder2);
        FileFactory.createImageFile("4",subFolder2);
        
        List<WebElement> elements = testObject.run(root);
        
        assertEquals("2016",elements.get(0).getYear());
        assertEquals("Dezember",elements.get(0).getHeaderName());
        assertEquals("4",elements.get(1).getText());
        assertEquals("2015",elements.get(2).getYear());
        assertEquals("Januar",elements.get(2).getHeaderName());
        assertEquals("Frankreich",elements.get(3).getHeaderName());
        assertEquals("Leipzig",elements.get(3).getHeaderSubname());
        assertEquals("Description",elements.get(3).getText());
        assertEquals("1",elements.get(4).getText());
        assertEquals("2",elements.get(5).getText());
        assertEquals("Frankreich",elements.get(6).getHeaderName());
        assertEquals("Merzien",elements.get(6).getHeaderSubname());
        assertEquals("3",elements.get(7).getText());
    }
    
    @Test
    public void testTransorm_withDepthFour(){
        File root = FileFactory.createRoot();
        File folderDepth1 = FileFactory.createFolder("Depth1",root);
        File folderDepth2 = FileFactory.createFolder("Depth2",folderDepth1);
        File folderDepth3 = FileFactory.createFolder("Depth3",folderDepth2);
        File folderDepth4 = FileFactory.createFolder("Depth4",folderDepth3);
        File textFile = FileFactory.createTextFile("!description", folderDepth4);
        textFile.setText("Description");
        
        List<WebElement> elements = testObject.run(root);
        
        assertEquals("Depth1",elements.get(0).getYear());
        assertEquals("Depth2",elements.get(0).getHeaderName());
        assertEquals("Depth3",elements.get(0).getHeaderSubname());
        assertEquals("Description",elements.get(0).getText());
    }
    
    @Test
    public void testTransorm_keyIsSet(){
        File root = FileFactory.createRoot();
        File folder = FileFactory.createFolder("2015",root);
        FileFactory.createImageFile("001.jpg", folder);
        
        List<WebElement> elements = testObject.run(root);
        
        assertFalse(elements.get(0).getId().isEmpty());
        assertFalse(elements.get(1).getId().isEmpty());
    }
    
    @Test
    public void testTransorm_parentKeyIsCorrect(){
        File root = FileFactory.createRoot();
        File folder1 = FileFactory.createFolder("2016",root);
        File folder2 = FileFactory.createFolder("2015",root);
        FileFactory.createImageFile("001.jpg", folder2);
        
        List<WebElement> elements = testObject.run(root);
        
        assertEquals(elements.get(1).getId(), elements.get(2).getHeaderId());
    }
}
