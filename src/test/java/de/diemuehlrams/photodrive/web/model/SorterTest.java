package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.files.FileFactory;
import java.util.Collections;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author larswolfram
 */
public class SorterTest {
    
    private Sorter sorter;
    
    @Before
    public void before(){
        sorter = new Sorter();
    }

    @Test
    public void testRun_null() {
        try {
            sorter.run(null);
        } catch (Exception ex) {
            assertTrue(false);
        }
    }

    @Test
    public void testRun_onlyRoot() {
        try {
            File root = FileFactory.createRoot();
            sorter.run(root);
        } catch (Exception ex) {
            assertTrue(false);
        }
    }

    @Test
    public void testRun_years() {
        File root = FileFactory.createRoot();
        FileFactory.createFolder("2012", root);
        FileFactory.createFolder("2013", root);
        FileFactory.createFolder("2014", root);
        Collections.shuffle(root.getChildren());

        sorter.run(root);
        assertEquals("2014", root.getChildren().get(0).getName());
        assertEquals("2013", root.getChildren().get(1).getName());
        assertEquals("2012", root.getChildren().get(2).getName());
    }

    @Test
    public void testRun_onlyMonth() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        FileFactory.createFolder("Januar", year);
        FileFactory.createFolder("Februar", year);
        FileFactory.createFolder("März", year);
        FileFactory.createFolder("April", year);
        FileFactory.createFolder("Mai", year);
        FileFactory.createFolder("Juni", year);
        FileFactory.createFolder("Juli", year);
        FileFactory.createFolder("August", year);
        FileFactory.createFolder("September", year);
        FileFactory.createFolder("Oktober", year);
        FileFactory.createFolder("November", year);
        FileFactory.createFolder("Dezember", year);
        Collections.shuffle(year.getChildren());

        sorter.run(root);
        assertEquals("Januar", year.getChildren().get(0).getName());
        assertEquals("Februar", year.getChildren().get(1).getName());
        assertEquals("März", year.getChildren().get(2).getName());
        assertEquals("April", year.getChildren().get(3).getName());
        assertEquals("Mai", year.getChildren().get(4).getName());
        assertEquals("Juni", year.getChildren().get(5).getName());
        assertEquals("Juli", year.getChildren().get(6).getName());
        assertEquals("August", year.getChildren().get(7).getName());
        assertEquals("September", year.getChildren().get(8).getName());
        assertEquals("Oktober", year.getChildren().get(9).getName());
        assertEquals("November", year.getChildren().get(10).getName());
        assertEquals("Dezember", year.getChildren().get(11).getName());
    }

    @Test
    public void testRun_monthAndOthers() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        FileFactory.createFolder("Januar", year);
        FileFactory.createFolder("Februar", year);
        FileFactory.createFolder("Aachen", year);
        FileFactory.createFolder("Zürich", year);
        Collections.shuffle(year.getChildren());
        
        sorter.run(root);
        assertEquals("Januar", year.getChildren().get(0).getName());
        assertEquals("Februar", year.getChildren().get(1).getName());
        assertEquals("Aachen", year.getChildren().get(2).getName());
        assertEquals("Zürich", year.getChildren().get(3).getName());
    }

    @Test
    public void testRun_monthAndOrderdOthers() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        FileFactory.createFolder("Januar", year);
        FileFactory.createFolder("Februar", year);
        FileFactory.createFolder("002. Aachen", year);
        FileFactory.createFolder("001. Zürich", year);
        Collections.shuffle(year.getChildren());
        
        sorter.run(root);
        assertEquals("Januar", year.getChildren().get(0).getName());
        assertEquals("Februar", year.getChildren().get(1).getName());
        assertEquals("002. Aachen", year.getChildren().get(2).getName());
        assertEquals("001. Zürich", year.getChildren().get(3).getName());
    }
    
    @Test
    public void testRun_filesAndFoldersDepth1() {
        File root = FileFactory.createRoot();
        FileFactory.createFolder("003", root);
        FileFactory.createImageFile("002.jpg", root);
        FileFactory.createImageFile("001.jpg", root);
        Collections.shuffle(root.getChildren());
        
        sorter.run(root);
        
        assertEquals("001.jpg", root.getChildren().get(0).getName());
        assertEquals("002.jpg", root.getChildren().get(1).getName());
        assertEquals("003", root.getChildren().get(2).getName());
    }
    
    @Test
    public void testRun_filesAndFoldersDepth2() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        File month = FileFactory.createFolder("Januar", year);
        FileFactory.createImageFile("002.jpg", year);
        FileFactory.createImageFile("001.jpg", year);
        FileFactory.createFolder("003", year);
        FileFactory.createFolder("002", year);
        Collections.shuffle(year.getChildren());
        
        sorter.run(root);
        assertEquals("001.jpg", year.getChildren().get(0).getName());
        assertEquals("002.jpg", year.getChildren().get(1).getName());
        assertEquals("Januar", year.getChildren().get(2).getName());
        assertEquals("003", year.getChildren().get(3).getName());
        assertEquals("002", year.getChildren().get(4).getName());
    }
    
    @Test
    public void testRun_filesAndFoldersDepth3() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        File month = FileFactory.createFolder("Januar", year);
        FileFactory.createImageFile("002.jpg", month);
        FileFactory.createImageFile("001.jpg", month);
        FileFactory.createFolder("003", month);
        Collections.shuffle(month.getChildren());
        
        sorter.run(root);
        assertEquals("001.jpg", month.getChildren().get(0).getName());
        assertEquals("002.jpg", month.getChildren().get(1).getName());
        assertEquals("003", month.getChildren().get(2).getName());
        
    }
    
    @Test
    public void testRun_onlyFiles() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        File month = FileFactory.createFolder("Januar", year);
        FileFactory.createImageFile("003.jpg", month);
        FileFactory.createImageFile("002.jpg", month);
        FileFactory.createImageFile("001.jpg", month);
        Collections.shuffle(month.getChildren());
        
        sorter.run(root);
        assertEquals("001.jpg", month.getChildren().get(0).getName());
        assertEquals("002.jpg", month.getChildren().get(1).getName());
        assertEquals("003.jpg", month.getChildren().get(2).getName());
    }
    
    @Test
    public void testRun_sortTimestamp() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        File month = FileFactory.createFolder("Januar", year);
        File file1 = FileFactory.createImageFile("P003.jpg", month);
        file1.setCreationTimestamp(1l);
        File file2 = FileFactory.createImageFile("P002.jpg", month);
        file2.setCreationTimestamp(2l);
        File file3 = FileFactory.createImageFile("P001.jpg", month);
        file3.setCreationTimestamp(3l);
        Collections.shuffle(month.getChildren());
        
        sorter.run(root);
        assertEquals("P003.jpg", month.getChildren().get(0).getName());
        assertEquals("P002.jpg", month.getChildren().get(1).getName());
        assertEquals("P001.jpg", month.getChildren().get(2).getName());
    }
    
     @Test
    public void testRun_sortTimestampAndName() {
        File root = FileFactory.createRoot();
        File year = FileFactory.createFolder("2012", root);
        File month = FileFactory.createFolder("Januar", year);
        File file1 = FileFactory.createTextFile("!description.txt", month);
        file1.setCreationTimestamp(1l);
        File file2 = FileFactory.createImageFile("001.jpg", month);
        file2.setCreationTimestamp(4l);
        File file3 = FileFactory.createImageFile("P001.jpg", month);
        file3.setCreationTimestamp(2l);
        File file4 = FileFactory.createImageFile("P002.jpg", month);
        file4.setCreationTimestamp(3l);
        Collections.shuffle(month.getChildren());
        
        sorter.run(root);
        assertEquals("!description.txt", month.getChildren().get(0).getName());
        assertEquals("001.jpg", month.getChildren().get(1).getName());
        assertEquals("P001.jpg", month.getChildren().get(2).getName());
        assertEquals("P002.jpg", month.getChildren().get(3).getName());
    }
}
